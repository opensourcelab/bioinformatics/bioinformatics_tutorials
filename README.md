# Bioinformatics

Tutorial material for bioinformatics lectures

Most of the material is provided as jupyter notebooks that you can interactivly use
(with working code included)
This requires an installation of *jupyter notebook* or *jupyter-lab*
and the python package dependencies (= python packages used to work with the notebooks) 

## Local installation of Jupyter

   please make sure, that python3.7 or newer is installed on your system
   
   On current Linux distributions this is the default
   
   On OSX and M$Windows, please install python from the app stores. 
   
 * **Virtal environment**
 
 It is highly recommended to install a virtual environment
 
         python3 -m venv bioinfo
     
     to activate the virtual you should write in
     
     **Linux / OSX**:
         source path_to_venv/bin/activate
         
     **M$Windows**:
         path_to_venv/bin/activate.bat
 
 * **Required libraries**
    with active virtual environment write:
    
       pip install -r requirements
    
 * **Registering the virtual env for the jupyter kernel**
 
       python -m ipykernel install --name=bioinfo  # name: just a reference name
 
## Jupyter-lab

To start the notebook server, activate your virtual environment and type:

     jupyter-lab
  
Once jupyter-lab is running, the 
 

 
